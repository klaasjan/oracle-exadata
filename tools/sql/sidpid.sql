set lines 174
set verify off

col sid form 99999999
col state form a10
col status form a10
col os_pid form a10

def v_sid='&2'
def v_column='&1'

prompt
prompt What : Get sid or OS pid from sid or OS pid :-)
prompt Usage: @sidpid <sid|spid> INT

select s.sid, s.status, s.username, s.program, s.serial#, p.spid os_pid, p.pid db_pid, s.state from v$session s, v$process p 
where p.addr=s.paddr
and &1='&v_sid'
order by 1;

undefine v_sid
undefine 1

set verify on
clear columns