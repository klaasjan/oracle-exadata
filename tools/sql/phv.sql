-- Display different execution plans based on SQL_ID
-- Frits
select snap_id, instance_number, plan_hash_value, sql_profile, px_servers_execs_delta, disk_reads_delta, buffer_gets_delta, rows_processed_delta, cpu_time_delta, elapsed_time_delta
from dba_hist_sqlstat where sql_id = '&sql_id'
/
