prompt Setting up oradebug...
oradebug setmypid
oradebug unlimit
prompt

prompt Running ASH dump...
oradebug dump ashdumpseconds 30
prompt

prompt Running hanganalyze level 3...
oradebug hanganalyze 3
prompt

prompt Running systemstate dump...
oradebug dump systemstate 266
prompt

prompt tracefile containing all data:
oradebug tracefile_name
