set verify off

set lines 199
set pages 500

select dbms_rowid.rowid_to_absolute_fno(rowid, 'SYS', 'HCC_ME') file_num,
       dbms_rowid.rowid_block_number(rowid, 'BIGFILE') block_num,
       dbms_rowid.rowid_row_number(rowid) row_num
from hcc_me where id = 1;

undefine 1
undefine 2

set verify on

