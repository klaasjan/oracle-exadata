define SQLID="&1"
define CHILD="&2"

set verify off

SELECT *
FROM TABLE(dbms_xplan.display_cursor(sql_id          => '&&sqlid', 
                                     cursor_child_no => (nvl('&&child', (SELECT MAX(child_number) FROM v$sql WHERE sql_id='%%sqlid'))),
                                     format          => 'ALLSTATS'));
undefine 1
undefine 2
undefine SQLID
undefine CHILD

set verify on
