rem params.sql -- list all parameters
rem simple script to find all parameters including underscore 
rem usage: params.sql <string> <false/true (default)>

set verify off

set lines 199
set pages 500

col name format a50
col value format a20
col type format a15
col description format a70

select a.ksppinm name,
       b.ksppstvl value,
       b.ksppstdf deflt,
       decode
         (a.ksppity, 
          1,'boolean', 
          2,'string', 
          3,'integer', 
          4,'file',
          5,'number',
          6,'big integer',
          a.ksppity) type,
       a.ksppdesc description
from
sys.x$ksppi a, sys.x$ksppcv b
where
       a.indx = b.indx
and    upper(a.ksppinm) like upper('%&&1%')
and    b.ksppstdf like (select upper(nvl('&&2','%E%')) from dual)
order by name;

undefine 1
undefine 2

set verify on



