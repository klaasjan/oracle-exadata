/* 
** Purpose:              This script is developed to be used by DBA's
**                       Script to list background processes and their
**                       status and description
**                       usage: @bgp <process name e.g. PMON>
*/

set lines 174
set verify off

col sid form 99999999
col name form a10
col state form a10
col status form a19
col os_pid form a10
col description form a70

def bg_name='&1'

select b.name, s.sid, s.status, s.serial#, p.spid os_pid, p.pid db_pid, s.state, b.description from v$bgprocess b, v$session s, v$process p 
where s.paddr=b.paddr
and p.addr=s.paddr
and upper(b.name) like upper('%&&bg_name%')
order by 1;

undefine bg_name
undefine 1

set verify on
clear columns
