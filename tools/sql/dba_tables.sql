/* 
** Purpose: Script pivots all columns in dba_tables
**
**          Usage: @dba_tables (owner).table_name
*/

set serveroutput on size 1000000
set verify off

declare

v_query varchar2(3000);
v_output varchar2(3000);
v_pad number;
v_tname_bind varchar2(100);             -- Bind variable placeholder
v_tname varchar2(100);                  -- Holds name of the table after regexp from v_tstring
v_towner varchar2(100);                 -- Holds name of the owner after regexp from v_tstring
v_towner_bind varchar2(100);            -- Bind variable placeholder
v_tstring varchar2(100) := '&1';        -- Holds the string of schema.table_name

begin

v_towner  := trim(trailing '.' from regexp_substr(v_tstring,'^.*\.'));
v_tname  := trim(leading '.' from regexp_substr(v_tstring,'\..*'));

if v_tname is NULL then
  v_tname := v_tstring;
end if; 

if v_towner is NULL then
  v_towner := '%';
end if; 

select max(length(column_name)) into v_pad from dba_tab_cols where table_name ='DBA_TABLES' order by column_id;

for col in (select column_name from dba_tab_cols where table_name ='DBA_TABLES' order by column_id) loop
v_query := 'select '||col.column_name||' from DBA_TABLES where upper(table_name) = upper(:v_tname_bind) and upper(owner) like upper(:v_towner_bind)';
execute immediate v_query into v_output using v_tname, v_towner;
dbms_output.put_line(rpad(col.column_name, v_pad, ' ')||' = '|| v_output);

end loop;

end;
/

