/*
** Purpose: Script pivots all columns in dba_segments
**
**          Usage: @dba_segments (owner).segment_name
*/

set serveroutput on size 1000000
set verify off

declare

v_query varchar2(3000);
v_output varchar2(3000);
v_pad number;
v_sname_bind varchar2(100);             -- Bind variable placeholder
v_sname varchar2(100);                  -- Holds name of the segment after regexp from v_sstring
v_sowner varchar2(100);                 -- Holds name of the owner after regexp from v_sstring
v_sowner_bind varchar2(100);            -- Bind variable placeholder
v_sstring varchar2(100) := '&1';        -- Holds the string of schema.segment_name

begin

v_sowner  := trim(trailing '.' from regexp_substr(v_sstring,'^.*\.'));
v_sname  := trim(leading '.' from regexp_substr(v_sstring,'\..*'));

if v_sname is NULL then
  v_sname := v_sstring;
end if; 

if v_sowner is NULL then
  v_sowner := '%';
end if; 

select max(length(column_name)) into v_pad from dba_tab_cols where table_name ='DBA_SEGMENTS' order by column_id;

for col in (select column_name from dba_tab_cols where table_name ='DBA_SEGMENTS' order by column_id) loop
v_query := 'select '||col.column_name||' from DBA_SEGMENTS where upper(segment_name) = upper(:v_sname_bind) and upper(owner) like upper(:v_sowner_bind)';
  execute immediate v_query into v_output using v_sname, v_sowner;
  dbms_output.put_line(rpad(col.column_name, v_pad, ' ')||' = '|| v_output);
end loop;

end;
/

