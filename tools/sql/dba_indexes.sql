/*
** Purpose: Script pivots all columns in dba_indexes
**
**          Usage: @dba_tables (owner).index_name
*/

set serveroutput on size 1000000
set verify off

declare

v_query varchar2(3000);                 -- holds query to run through the columns
v_output varchar2(3000);                -- buffers output of the query to print
v_pad number;                           -- Holds size of largest column so we can align everything
v_iname_bind varchar2(100);             -- Bind variable placeholder
v_iname varchar2(100);                  -- Holds name of the index after regexp from v_istring
v_iowner varchar2(100);                 -- Holds name of the owner after regexp from v_istring
v_iowner_bind varchar2(100);            -- Bind variable placeholder
v_istring varchar2(100) := '&1';        -- Holds the string of schema.index_name

begin

v_iowner  := trim(trailing '.' from regexp_substr(v_istring,'^.*\.'));
v_iname  := trim(leading '.' from regexp_substr(v_istring,'\..*'));

if v_iname is NULL then
  v_iname := v_istring;
end if; 

if v_iowner is NULL then
  v_iowner := '%';
end if; 

select max(length(column_name)) into v_pad from dba_tab_cols where table_name ='DBA_INDEXES' order by column_id;

for col in (select column_name from dba_tab_cols where table_name ='DBA_INDEXES' order by column_id) loop
v_query := 'select '||col.column_name||' from DBA_INDEXES where upper(index_name) = upper(:v_iname_bind) and upper(owner) like upper(:v_iowner_bind)';
execute immediate v_query into v_output using v_iname, v_iowner;
dbms_output.put_line(rpad(col.column_name, v_pad, ' ')||' = '|| v_output);

end loop;

end;
/