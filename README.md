# Exadata Toolset

## Exadata toolset

Originally created and maintained by Klaas-Jan Jongsma and Frits Hoogland

This scriptset contains a selection of script for management of an Oracle database environment. The scriptset will work fine on any environment but the focus is mainly exadata nowadays.

1. Installation
2. What profile arranges for you

## 1. Installation
If you've received or downloaded tools as a gzipped tarball, upload it to a unix/linux server you want this to work on, and log in as the 'oracle' user. Next, make sure '/u01' is writable for 'oracle', and if so, install it eg.:
```bash
$ tar xzf ~/tools.tgz -C /u01
```

By default, the scripts expects to be installed in /u01/tools, if you want to install your script at a different locaction change the variable of TOOLS\_HOME in $TOOLS\_HOME/bin/profile at the top of the script. 

In order to get everything setup in the bash shell, add the following line to ~/.bash_profile:
```bash
. /u01/tools/bin/profile
```

Make sure that there is a softlink between the oratab to the and $TOOLS\_HOME/etc directory:
```bash
$ ln -sf /etc/oratab /u01/tools/etc/oratab
```

If the 'rlwrap' utility is not installed (which isn't by default), install it using the rpm (RHEL/OL 5 or 6 x86\_64 only) in /u01/tools/rpm/x86_64.
Additionally, you could install a few more rpm's if you wish, most notably the 'screen' package, to make your session survive network failure. Screen seems to have made the OL repository with OL6.
Please mind installing rpm packages requires root privileges, either via becoming root, or via privileges delegated via sudo.

That's all, now if you create a new session, tools will show you the currently installed databases (via /u01/tools/etc/oratab), if they are up or down, and have all the aliasses in place.

## 2. What profile arranges for you
If you log in, the /u01/tools/bin/profile script is executed if you log in via a terminal. If TERM starts with 'xterm', profile should understand. If your terminal is a little more exotic, profile will not understand you are a terminal, and display nothing. In order to get that fixed, you could either get your terminal type set to xterm, or modify the source to include your terminal.

The first row profile outputs displays the hostname, architecture and operating system. This is to make sure you've logged on to the correct machine, and understand on what type of machine you are.
The second row displays the output of the uptime command. Most important are the load figures here, so you get an understanding of the business of the operating system.

If oraenv is not installed in /usr/local/bin, profile will generate a message. It will not stop, and continue to execute. 
The next steps it reads the /u01/tools/etc/oratab link to your oratab file, and display the database names in it, and if they are up or down (done in a simple way by looking if the pmon process exist for the database name), and generating a set of shell aliases in order to set the environment.

The aliasses use the default, generic way to set the environment (set ORACLE\_SID and ORACLE\_HOME) by executing oraenv. 

If you've looked closely, you see that tools generated an extra set of brackets in the shell prompt: []. If you set an database environment by typing the database name, the INSTANCE name (ORACLE\_SID) is placed between the brackets. Additionally, the present working directory, ORACLE\_SID and ORACLE\_HOME are displayed in the window header too.

If an environment is set, the ORACLE\_HOME is added to the CDPATH shell variable, along with the trace directory of the instance in the diagnostics repository. This means you can jump to any directory in the ORACLE\_HOME or the database or ASM diag dest home from anywhere.

## Aliases
A besides all the database settings, a set of aliasses is added to make common administration commands shorter:

| Alias | Description |
| -------- |-------------|
|rs        | rlwrap sqlplus |
|sq        |rs '/ as sysdba'
|sqa       |rs '/ as sysasm'
|cstat     |ASM\_HOME/bin/crsctl status resource -t
|rr        |rlwrap rman target /
|rd        |rlwrap dgmgrl /
|oh        |cd $ORACLE\_HOME
|reprofile |execute bin/profile again, mostly for re-reading oratab



